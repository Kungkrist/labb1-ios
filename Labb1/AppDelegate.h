//
//  AppDelegate.h
//  Labb1
//
//  Created by DEA on 2016-01-28.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

