//
//  main.m
//  Labb1
//
//  Created by DEA on 2016-01-28.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
